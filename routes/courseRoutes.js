const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require('../auth.js');

//Rout for creating  a course
// router.post("/", auth.verify, (req, res)=> {
// 	courseController.addCourse(req.body). then(resultFromController => res.send(resultFromController));
// });



/*
	Mini Activity: 
	Limit the course creation to admin only. Refactor the course route/controller.


*/
//Activity
//route for creating a course
// router.post("/", auth.verify, (req, res)=> {
// 	const userData = auth.decode(req.headers.authorization);
// 	if(userData.isAdmin) {
// 		courseController.addCourse(req.body).then(resultFromController => res.send(
// 			resultFromController));
// 	} else {
// 		return res.send("User is not  Admin");
// 	}
// });
//---------------------------
//Route for retrieving all the courses
router.get("/all", (req, res)=> {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});



//---------------------------------------------------------------------
//activity ni maam

//Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdmin);

	if(isAdmin) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		
		res.send(false);
	}
	

});

//Route for retrieving all the active courses
router.get("/", (req, res)=> {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});



//Routes for retrieving specific course

router.get("/:courseId", (req, res)=>{
	console.log(req.params)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})


//Route for Updating a Course
router.put("/:courseId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		courseController.updateCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}

});



//-------------------------------------------------------------------
//activity s40
//Route for archiving course

router.put("/:courseId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.archiveCourse(req.params, req.body, userData).then(resultFromController => res.send(resultFromController));

});





















module.exports = router;